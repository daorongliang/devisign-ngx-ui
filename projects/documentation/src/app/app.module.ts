
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DeviToastModule, DeviGridModule, DeviBackdropModule } from 'devisign-ngx-ui';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    DeviToastModule,
    DeviGridModule,
    DeviBackdropModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
