import { Component } from '@angular/core';
import { DeviToast, DeviBackdrop } from 'devisign-ngx-ui';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'documentation';

  constructor(
    private _toast: DeviToast,
    private _backdrop: DeviBackdrop
  ) {
  }

  showLoading() {
    this._toast.showLoading();
    setTimeout(() => {
      this._toast.hide();
    }, 2000);
  }
}
