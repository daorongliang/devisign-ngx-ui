# DEVISIGN-NGX-UI (In deveopment, do not use yet.)

DEVISIGN-NGX-UI is a Angular UI library.

[Gitlab URL](https://gitlab.com/daorongliang/devisign-ngx-ui): https://gitlab.com/daorongliang/devisign-ngx-ui


VS Code Snippets available by searching: devisign-ngx-ui

## Installation

Use the package manager [npm](https://www.npmjs.com/package/devisign-ngx-ui) to install.

```bash
npm i devisign-ngx-ui@latest
```

## Usage

```ts
// Availabel Modules:
// - GridModule
// - Toast Module

//*.module.ts
...
imports: [
  DeviToastModule,
  DeviGridModule
]
...

```

```html
<!--*.component.html-->
<d-row>
  <d-col xs12>
  </d-col>
</d-row>

```


```ts
//*.component.ts
  constructor(
    private _toast: DeviToast
  ) {
  ...

  this._toast.showLoading();
  this._toast.toast();
  this._toast.hide();

```

```
Upcoming featrues:
- Components:
  - Backdrop
  - Menu

- Service
  - Communicator

- Directives and Pipes
  - Colours Directive
  - Language Pipe

```


## Contributing
Join requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
