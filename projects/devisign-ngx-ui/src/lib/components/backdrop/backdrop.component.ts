import { Component, OnInit, ElementRef, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'd-backdrop',
  templateUrl: './backdrop.component.html',
  styleUrls: ['./backdrop.component.less']
})
export class BackdropComponent implements OnInit {

  @Output() clicked = new EventEmitter();

  constructor(
    private _el: ElementRef
  ) { }

  ngOnInit() {
  }

  show(opacity = 0.7, zIndex = 1000) {
    this._el.nativeElement.style.display = 'unset';
    this._el.nativeElement.style.opacity = opacity;
    this._el.nativeElement.style['z-index'] = zIndex;
  }

  hide() {
    this._el.nativeElement.style.opacity = 0;
    this._el.nativeElement.style['z-index'] = -1;
    setTimeout(() => {
      this.clicked.emit();
      this._el.nativeElement.style.display = 'none';
    }, 200);
  }

}
