import { DOCUMENT } from '@angular/common';
import { RenderService } from '../../services/render.service';
import { BackdropComponent } from './backdrop.component';
import {
  ApplicationRef,
  ComponentFactoryResolver,
  Inject,
  Injector,
  Injectable,
} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DeviBackdrop extends RenderService{

  constructor(
    _resolver: ComponentFactoryResolver,
    _applicationRef: ApplicationRef,
    _injector: Injector,
    @Inject(DOCUMENT) _document: any,
  ) {
    super(_resolver, _applicationRef, _injector, _document);
  }

  private componentRef;

  show(opacity: number = 0.7, zIndex: number = 1000) {
    console.log('show');
    this.componentRef = this.render(BackdropComponent);
    this.componentRef.instance.show(opacity, zIndex);
  }

  hide() {
    if (!this.componentRef) return ;

    this.componentRef.instance.hide();
    setTimeout(() => {
      this.destroy(this.componentRef);
    }, 200);
  }
}
