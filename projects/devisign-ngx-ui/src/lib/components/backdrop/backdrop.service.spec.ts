import { TestBed } from '@angular/core/testing';

import { DeviBackdrop } from './backdrop.service';

describe('BackdropService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeviBackdrop = TestBed.get(DeviBackdrop);
    expect(service).toBeTruthy();
  });
});
