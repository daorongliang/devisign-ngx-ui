import { TestBed } from '@angular/core/testing';

import { DeviToast } from './toast.service';

describe('DeviToast', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeviToast = TestBed.get(DeviToast);
    expect(service).toBeTruthy();
  });
});
