import { Component, OnInit } from '@angular/core';
import $ from 'jquery';

@Component({
  selector: 'd-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.css', './../../css/weui.css']
})
export class ToastComponent implements OnInit {

  public msg = 'Completed';
  public icon = 'done';

  public loading = false;

  constructor() { }

  ngOnInit() {
  }

  /**
   * Show Toast
   *
   * @param [msg] Message
   * @param [duration] Duration (ms) default: 1500
   * @param [type] Types ('loading' or 'icon') default: icon
   * @param [icon] Material Icon
   */
  toast(msg: string, duration: number = 1500, type: 'icon' | 'loading' = 'icon', icon: string = 'done') {
    const toastRef = $('#toast');
    this.msg = msg;
    this.loading = type === 'loading';
    this.icon = type === 'loading' ? '' : icon;

    if (toastRef.css('display') != 'none') return;
    toastRef.fadeIn(100);

    if (duration) {
      setTimeout(function () {
        toastRef.fadeOut(100);
      }, duration);
    }
  }

  fadeout() {
    $('#toast').fadeOut(100);
  }
}
