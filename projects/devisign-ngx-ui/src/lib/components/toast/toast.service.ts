import { ToastComponent } from './toast.component';
import { RenderService } from '../../services/render.service';
import { DOCUMENT } from '@angular/common';
import {
  ApplicationRef,
  ComponentFactoryResolver,
  Inject,
  Injector,
  Injectable,
} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DeviToast extends RenderService {

  constructor(
    _resolver: ComponentFactoryResolver,
    _applicationRef: ApplicationRef,
    _injector: Injector,
    @Inject(DOCUMENT) _document: any,
  ) {
    super(_resolver, _applicationRef, _injector, _document);
  }

  private componentRef;

  /**
   * Show Toast
   *
   * @param [msg] Message
   * @param [duration] Duration (ms) default: 1500
   * @param [type] Types ('loading' or 'icon') default: icon
   * @param [icon] Material Icon
   */
  toast(msg: string, duration: number = 1500, type: 'icon' | 'loading' = 'icon', icon: string = 'done') {
    this.destroyAll();
    setTimeout(() => {
      this.componentRef = this.render(ToastComponent);
      return this.componentRef.instance.toast(msg, duration, type, icon);
    }, 0);
  }

  showLoading() {
    this.destroyAll();
    setTimeout(() => {
      this.componentRef = this.render(ToastComponent);
      return this.componentRef.instance.toast('Loading', 0, 'loading');
    }, 0);
  }

  /**
   * Remove all toasts
   */
  hide() {
    if (this.componentRef)
      this.componentRef.instance.fadeout();

    setTimeout(() => {
      this.destroyAll();
    }, 100);
  }

}
