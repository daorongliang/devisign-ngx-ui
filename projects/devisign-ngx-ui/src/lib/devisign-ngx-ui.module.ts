import { DeviBackdropModule } from './components/backdrop/backdrop.module';
import { DeviLayoutModule } from './layout/layout.module';
import { NgModule } from '@angular/core';
import { DeviToastModule } from './components/toast/toast.module';
import { DeviGridModule } from './grid/grid.module';

@NgModule({
  imports: [
    DeviToastModule,
    DeviGridModule,
    DeviBackdropModule
  ],
  exports: [
    DeviToastModule,
    DeviGridModule,
    DeviBackdropModule
  ]
})
export class DeviNgxUiModule { }
