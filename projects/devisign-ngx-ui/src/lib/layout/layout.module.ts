import { HeaderComponent } from './header/header.component';
import { PageContentComponent } from './page-content/page-content.component';
import { PageMenuComponent } from './page-menu/page-menu.component';
import { PageComponent } from './page/page.component';
import { CommonModule } from '@angular/common';
import { NgModule, } from '@angular/core';
import { DeviBackdropModule } from '../components/backdrop/backdrop.module';

const components = [
  PageComponent,
  PageMenuComponent,
  PageContentComponent,
  HeaderComponent
]

@NgModule({
  imports: [
    CommonModule,
    DeviBackdropModule
  ],
  declarations: components,
  exports: [
    ...components
  ]
})
export class DeviLayoutModule {}
