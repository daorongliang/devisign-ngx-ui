import { BackdropComponent } from './../../components/backdrop/backdrop.component';
import { Component, OnInit, HostListener, Input, ViewChild, ElementRef, ComponentRef } from '@angular/core';
import { DeviBackdrop } from '../../components/backdrop/backdrop.service';

@Component({
  selector: 'd-page-menu',
  templateUrl: './page-menu.component.html',
  styleUrls: ['./page-menu.component.css']
})
export class PageMenuComponent implements OnInit {

  @Input() breakPoint: number = 600;

  @Input() mode: string = '';
  @Input() position: string = 'left';

  @Input() backdrop: boolean = true;

  @ViewChild('menu', {static: true}) menu: ElementRef<any>;
  @ViewChild('backdrop', {static: true}) _backdrop: ComponentRef<BackdropComponent>;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (this.mode === 'overlay') this.hide();
    else if (event.target.innerWidth < this.breakPoint)
      this.hide();
    else if (this.isHidden())
      this.show();
  }

  constructor(
  ) {
  }

  ngOnInit() {
    if (this.mode === 'overlay') this.hide();
    else if (window.innerWidth < this.breakPoint)
      this.hide();
    else if (this.isHidden())
      this.show();

    if (this.mode)
      this.menu.nativeElement.classList.add(this.mode);

    this.menu.nativeElement.style[this.position] = 0;
  }

  isHidden(): boolean {
    return this.menu.nativeElement.classList.contains('hide');
  }

  show(): void {
    this.menu.nativeElement.classList.remove('hide');
    if (this.backdrop && this.mode === 'overlay') this._backdrop.instance.show();
  }

  hide(): void {
    this.menu.nativeElement.classList.add('hide');
    if (this.backdrop && this.mode === 'overlay') this._backdrop.instance.hide();
  }

  toggle(): void {
    if (this.isHidden())
      this.show();
    else this.hide()
  }

}
