import { Component, OnInit, HostListener, Input, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'd-page-content',
  templateUrl: './page-content.component.html',
  styleUrls: ['./page-content.component.css']
})
export class PageContentComponent implements OnInit {

  @Input() breakPoint: number = 600;
  @ViewChild('content', {static: true}) content: ElementRef;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    event.target.innerWidth;
    if (event.target.innerWidth < this.breakPoint)
      this.hide();
    else if (this.isHidden())
      this.show();
  }
  constructor(
    private _el: ElementRef
  ) {
    console.log(_el.nativeElement)
  }

  ngOnInit() {
    if (window.innerWidth < this.breakPoint)
      this.hide();
    else if (this.isHidden())
      this.show();
  }

  isHidden(): boolean {
    return this.content.nativeElement.classList.contains('full-content');
  }

  show(): void {
    this.content.nativeElement.classList.remove('full-content');
  }

  hide(): void {
    this.content.nativeElement.classList.add('full-content');
  }

}
