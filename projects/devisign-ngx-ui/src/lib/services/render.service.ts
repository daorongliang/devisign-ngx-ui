import { DOCUMENT } from '@angular/common';
import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  EmbeddedViewRef,
  Inject,
  Injector,
} from '@angular/core';

export abstract class RenderService {

  protected list: Array<ComponentRef<any>> = [];

  constructor(
    private _resolver: ComponentFactoryResolver,
    private _applicationRef: ApplicationRef,
    private _injector: Injector,
    @Inject(DOCUMENT) private _document: any,
  ) { }


  /**
   * @param component Destroy the nth || newest component
   */
  destroy(component?: number | ComponentRef<any>) {
    if (!this.list.length) return ;

    if (typeof component === 'number') component = this.list[component as number];
    if (!component) component = this.list.pop();
    if (component) (component as ComponentRef<any>).destroy();
  }

  /**
   * Destroy ALl Components
   */
  destroyAll() {
    for (const component of this.list) this.destroy(component);
  }


  /**
   * Render Components
   */
  protected render<T>(component: new (...args: any[]) => T): ComponentRef<T> {
    const componentFactory = this._resolver.resolveComponentFactory(component);
    const componentRef = componentFactory.create(this._injector);
    this.list.push(componentRef);
    const componentRootNode = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    this._applicationRef.attachView(componentRef.hostView);
    componentRef.onDestroy(() => {
      this._applicationRef.detachView(componentRef.hostView);
    });
    this._document.body.appendChild(componentRootNode);
    return componentRef;
  }
}
