import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RowComponent } from './row/row.component';
import { ColComponent } from './col/col.component';
import { RowDirective } from './row/row.directive';

import { XsModule } from './col/xs/xs.module'
import { SmModule } from './col/sm/sm.module';
import { MdModule } from './col/md/md.module';
import { LgModule } from './col/lg/lg.module';
import { XlModule } from './col/xl/xl.module';

const components = [
  RowComponent,
  ColComponent,
  RowDirective
];

@NgModule({
  imports: [
    CommonModule,
    XsModule,
    SmModule,
    MdModule,
    LgModule,
    XlModule
  ],
  declarations: components,
  exports: [
    ...components,
    XsModule,
    SmModule,
    MdModule,
    LgModule,
    XlModule
  ]
})
export class DeviGridModule {}
