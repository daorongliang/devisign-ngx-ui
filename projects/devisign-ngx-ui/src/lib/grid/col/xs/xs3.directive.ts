import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xs3]'
})
export class Xs3Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-3');
  }
}
