import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xs6]'
})
export class Xs6Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-6');
  }
}
