import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xs8]'
})
export class Xs8Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-8');
  }
}
