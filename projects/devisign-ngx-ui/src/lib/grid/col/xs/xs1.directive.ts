import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xs1]'
})
export class Xs1Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-1');
  }
}
