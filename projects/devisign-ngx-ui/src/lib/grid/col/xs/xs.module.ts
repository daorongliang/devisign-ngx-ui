import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Xs1Directive } from './xs1.directive';
import { Xs2Directive } from './xs2.directive';
import { Xs3Directive } from './xs3.directive';
import { Xs4Directive } from './xs4.directive';
import { Xs5Directive } from './xs5.directive';
import { Xs6Directive } from './xs6.directive';
import { Xs7Directive } from './xs7.directive';
import { Xs8Directive } from './xs8.directive';
import { Xs9Directive } from './xs9.directive';
import { Xs10Directive } from './xs10.directive';
import { Xs11Directive } from './xs11.directive';
import { Xs12Directive } from './xs12.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [Xs1Directive, Xs2Directive, Xs3Directive, Xs4Directive, Xs5Directive, Xs6Directive, Xs7Directive, Xs8Directive, Xs9Directive, Xs10Directive, Xs11Directive, Xs12Directive],
  exports: [Xs1Directive, Xs2Directive, Xs3Directive, Xs4Directive, Xs5Directive, Xs6Directive, Xs7Directive, Xs8Directive, Xs9Directive, Xs10Directive, Xs11Directive, Xs12Directive]
})
export class XsModule {}
