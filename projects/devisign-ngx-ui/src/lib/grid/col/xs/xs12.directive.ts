import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xs12]'
})
export class Xs12Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-12');
  }
}
