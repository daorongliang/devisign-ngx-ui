import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xs10]'
})
export class Xs10Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-10');
  }
}
