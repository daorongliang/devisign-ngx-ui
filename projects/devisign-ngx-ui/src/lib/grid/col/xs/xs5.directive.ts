import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xs5]'
})
export class Xs5Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-5');
  }
}
