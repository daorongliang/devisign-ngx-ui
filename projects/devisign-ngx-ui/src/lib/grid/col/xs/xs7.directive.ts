import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xs7]'
})
export class Xs7Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-7');
  }
}
