import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xs11]'
})
export class Xs11Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-11');
  }
}
