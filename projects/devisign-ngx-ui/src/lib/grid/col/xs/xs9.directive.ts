import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xs9]'
})
export class Xs9Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-9');
  }
}
