import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xs2]'
})
export class Xs2Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-2');
  }
}
