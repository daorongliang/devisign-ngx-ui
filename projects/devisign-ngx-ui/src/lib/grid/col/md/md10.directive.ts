import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[md10]'
})
export class Md10Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-md-10');
  }
}
