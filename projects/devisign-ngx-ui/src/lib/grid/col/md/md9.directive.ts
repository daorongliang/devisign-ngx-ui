import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[md9]'
})
export class Md9Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-md-9');
  }
}
