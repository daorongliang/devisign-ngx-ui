import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[md12]'
})
export class Md12Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-md-12');
  }
}
