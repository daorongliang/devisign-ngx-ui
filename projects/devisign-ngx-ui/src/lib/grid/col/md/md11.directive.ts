import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[md11]'
})
export class Md11Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-md-11');
  }
}
