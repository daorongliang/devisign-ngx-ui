import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[md3]'
})
export class Md3Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-md-3');
  }
}
