import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[md4]'
})
export class Md4Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-md-4');
  }
}
