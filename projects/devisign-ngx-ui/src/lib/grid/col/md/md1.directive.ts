import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[md1]'
})
export class Md1Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-md-1');
  }
}
