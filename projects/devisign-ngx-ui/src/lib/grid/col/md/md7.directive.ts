import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[md7]'
})
export class Md7Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-md-7');
  }
}
