import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[md2]'
})
export class Md2Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-md-2');
  }
}
