import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[md8]'
})
export class Md8Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-md-8');
  }
}
