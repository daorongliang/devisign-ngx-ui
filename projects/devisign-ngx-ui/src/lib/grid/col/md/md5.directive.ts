import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[md5]'
})
export class Md5Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-md-5');
  }
}
