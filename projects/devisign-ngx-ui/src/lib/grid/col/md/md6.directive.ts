import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[md6]'
})
export class Md6Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-md-6');
  }
}
