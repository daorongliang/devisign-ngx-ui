import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Md1Directive } from './md1.directive';
import { Md2Directive } from './md2.directive';
import { Md3Directive } from './md3.directive';
import { Md4Directive } from './md4.directive';
import { Md5Directive } from './md5.directive';
import { Md6Directive } from './md6.directive';
import { Md7Directive } from './md7.directive';
import { Md8Directive } from './md8.directive';
import { Md9Directive } from './md9.directive';
import { Md10Directive } from './md10.directive';
import { Md11Directive } from './md11.directive';
import { Md12Directive } from './md12.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [Md1Directive, Md2Directive, Md3Directive, Md4Directive, Md5Directive, Md6Directive, Md7Directive, Md8Directive, Md9Directive, Md10Directive, Md11Directive, Md12Directive],
  exports: [Md1Directive, Md2Directive, Md3Directive, Md4Directive, Md5Directive, Md6Directive, Md7Directive, Md8Directive, Md9Directive, Md10Directive, Md11Directive, Md12Directive]
})
export class MdModule {}
