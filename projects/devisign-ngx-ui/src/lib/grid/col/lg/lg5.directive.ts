import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[lg5]'
})
export class Lg5Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-lg-5');
  }
}
