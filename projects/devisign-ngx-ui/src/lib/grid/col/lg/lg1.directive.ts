import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[lg1]'
})
export class Lg1Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-lg-1');
  }
}
