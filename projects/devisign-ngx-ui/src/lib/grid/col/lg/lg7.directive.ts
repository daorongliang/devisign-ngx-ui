import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[lg7]'
})
export class Lg7Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-lg-7');
  }
}
