import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[lg11]'
})
export class Lg11Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-lg-11');
  }
}
