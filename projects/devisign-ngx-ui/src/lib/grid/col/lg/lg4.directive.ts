import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[lg4]'
})
export class Lg4Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-lg-4');
  }
}
