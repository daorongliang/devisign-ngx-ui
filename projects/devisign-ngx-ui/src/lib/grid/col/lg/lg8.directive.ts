import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[lg8]'
})
export class Lg8Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-lg-8');
  }
}
