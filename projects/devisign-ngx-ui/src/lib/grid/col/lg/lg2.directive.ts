import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[lg2]'
})
export class Lg2Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-lg-2');
  }
}
