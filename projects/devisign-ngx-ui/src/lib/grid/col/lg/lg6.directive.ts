import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[lg6]'
})
export class Lg6Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-lg-6');
  }
}
