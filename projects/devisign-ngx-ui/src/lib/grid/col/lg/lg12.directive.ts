import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[lg12]'
})
export class Lg12Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-lg-12');
  }
}
