import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[lg3]'
})
export class Lg3Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-lg-3');
  }
}
