import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[lg10]'
})
export class Lg10Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-lg-10');
  }
}
