import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Lg1Directive } from './lg1.directive';
import { Lg2Directive } from './lg2.directive';
import { Lg3Directive } from './lg3.directive';
import { Lg4Directive } from './lg4.directive';
import { Lg5Directive } from './lg5.directive';
import { Lg6Directive } from './lg6.directive';
import { Lg7Directive } from './lg7.directive';
import { Lg8Directive } from './lg8.directive';
import { Lg9Directive } from './lg9.directive';
import { Lg10Directive } from './lg10.directive';
import { Lg11Directive } from './lg11.directive';
import { Lg12Directive } from './lg12.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [Lg1Directive, Lg2Directive, Lg3Directive, Lg4Directive, Lg5Directive, Lg6Directive, Lg7Directive, Lg8Directive, Lg9Directive, Lg10Directive, Lg11Directive, Lg12Directive],
  exports: [Lg1Directive, Lg2Directive, Lg3Directive, Lg4Directive, Lg5Directive, Lg6Directive, Lg7Directive, Lg8Directive, Lg9Directive, Lg10Directive, Lg11Directive, Lg12Directive]
})
export class LgModule {}
