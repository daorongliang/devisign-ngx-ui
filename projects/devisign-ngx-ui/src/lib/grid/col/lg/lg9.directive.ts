import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[lg9]'
})
export class Lg9Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-lg-9');
  }
}
