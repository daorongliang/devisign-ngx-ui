import { Component, OnInit, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'd-col',
  templateUrl: './col.component.html',
  styleUrls: ['./col.component.css']
})
export class ColComponent implements OnInit {
  @Input() col:number | string = 0;
  @Input() xs:number | string = 0;
  @Input() sm:number | string = 0;
  @Input() md:number | string = 0;
  @Input() lg:number | string = 0;
  @Input() xl:number | string = 0;

  constructor(private _elRef:ElementRef) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      if (this.col && !isNaN(Number(this.col))) this._elRef.nativeElement.classList.add("col-" + this.col);
      if (this.xs && !isNaN(Number(this.xs))) this._elRef.nativeElement.classList.add("col-xs-" + this.xs);
      if (this.sm && !isNaN(Number(this.sm))) this._elRef.nativeElement.classList.add("col-sm-" + this.sm);
      if (this.md && !isNaN(Number(this.md))) this._elRef.nativeElement.classList.add("col-md-" + this.md);
      if (this.lg && !isNaN(Number(this.lg))) this._elRef.nativeElement.classList.add("col-lg-" + this.lg);
      if (this.xl && !isNaN(Number(this.xl))) this._elRef.nativeElement.classList.add("col-xl-" + this.xl);
    }, 0);
  }

}
