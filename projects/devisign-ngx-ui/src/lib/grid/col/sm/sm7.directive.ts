import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[sm7]'
})
export class Sm7Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-sm-7');
  }
}
