import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[sm3]'
})
export class Sm3Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-sm-3');
  }
}
