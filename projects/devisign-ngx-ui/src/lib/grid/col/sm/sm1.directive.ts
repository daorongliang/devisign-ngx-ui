import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[sm1]'
})
export class Sm1Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-sm-1');
  }
}
