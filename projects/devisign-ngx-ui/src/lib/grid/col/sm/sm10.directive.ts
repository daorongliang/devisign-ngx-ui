import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[sm10]'
})
export class Sm10Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-sm-10');
  }
}
