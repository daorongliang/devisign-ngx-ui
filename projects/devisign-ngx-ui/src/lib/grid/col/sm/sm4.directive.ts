import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[sm4]'
})
export class Sm4Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-sm-4');
  }
}
