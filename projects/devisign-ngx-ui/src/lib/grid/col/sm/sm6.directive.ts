import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[sm6]'
})
export class Sm6Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-sm-6');
  }
}
