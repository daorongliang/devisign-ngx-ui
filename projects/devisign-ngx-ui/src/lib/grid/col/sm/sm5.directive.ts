import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[sm5]'
})
export class Sm5Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-sm-5');
  }
}
