import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Sm1Directive } from './sm1.directive';
import { Sm2Directive } from './sm2.directive';
import { Sm3Directive } from './sm3.directive';
import { Sm4Directive } from './sm4.directive';
import { Sm5Directive } from './sm5.directive';
import { Sm6Directive } from './sm6.directive';
import { Sm7Directive } from './sm7.directive';
import { Sm8Directive } from './sm8.directive';
import { Sm9Directive } from './sm9.directive';
import { Sm10Directive } from './sm10.directive';
import { Sm11Directive } from './sm11.directive';
import { Sm12Directive } from './sm12.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [Sm1Directive, Sm2Directive, Sm3Directive, Sm4Directive, Sm5Directive, Sm6Directive, Sm7Directive, Sm8Directive, Sm9Directive, Sm10Directive, Sm11Directive, Sm12Directive],
  exports: [Sm1Directive, Sm2Directive, Sm3Directive, Sm4Directive, Sm5Directive, Sm6Directive, Sm7Directive, Sm8Directive, Sm9Directive, Sm10Directive, Sm11Directive, Sm12Directive]
})
export class SmModule {}
