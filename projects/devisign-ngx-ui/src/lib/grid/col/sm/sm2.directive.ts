import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[sm2]'
})
export class Sm2Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-sm-2');
  }
}
