import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[sm9]'
})
export class Sm9Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-sm-9');
  }
}
