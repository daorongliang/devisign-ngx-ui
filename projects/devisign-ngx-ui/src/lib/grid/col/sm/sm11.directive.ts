import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[sm11]'
})
export class Sm11Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-sm-11');
  }
}
