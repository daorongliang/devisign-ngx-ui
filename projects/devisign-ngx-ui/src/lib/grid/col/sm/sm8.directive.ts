import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[sm8]'
})
export class Sm8Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-sm-8');
  }
}
