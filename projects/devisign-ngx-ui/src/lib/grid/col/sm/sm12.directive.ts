import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[sm12]'
})
export class Sm12Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-sm-12');
  }
}
