import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xl8]'
})
export class Xl8Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-xl-8');
  }
}
