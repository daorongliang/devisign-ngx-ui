import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[libXl3]'
})
export class Xl3Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-xl-3');
  }
}
