import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xl1]'
})
export class Xl1Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-xl-1');
  }
}
