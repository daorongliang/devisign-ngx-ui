import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xl10]'
})
export class Xl10Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-xl-11');
  }
}
