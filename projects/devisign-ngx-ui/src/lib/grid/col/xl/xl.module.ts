import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Xl1Directive } from './xl1.directive';
import { Xl2Directive } from './xl2.directive';
import { Xl3Directive } from './xl3.directive';
import { Xl4Directive } from './xl4.directive';
import { Xl5Directive } from './xl5.directive';
import { Xl6Directive } from './xl6.directive';
import { Xl7Directive } from './xl7.directive';
import { Xl8Directive } from './xl8.directive';
import { Xl9Directive } from './xl9.directive';
import { Xl10Directive } from './xl10.directive';
import { Xl11Directive } from './xl11.directive';
import { Xl12Directive } from './xl12.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [Xl1Directive, Xl2Directive, Xl3Directive, Xl4Directive, Xl5Directive, Xl6Directive, Xl7Directive, Xl8Directive, Xl9Directive, Xl10Directive, Xl11Directive, Xl12Directive],
  exports: [Xl1Directive, Xl2Directive, Xl3Directive, Xl4Directive, Xl5Directive, Xl6Directive, Xl7Directive, Xl8Directive, Xl9Directive, Xl10Directive, Xl11Directive, Xl12Directive]
})
export class XlModule {}
