import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xl5]'
})
export class Xl5Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-xl-5');
  }
}
