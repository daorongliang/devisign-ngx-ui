import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xl6]'
})
export class Xl6Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-xl-6');
  }
}
