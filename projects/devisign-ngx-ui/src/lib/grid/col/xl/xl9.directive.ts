import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xl9]'
})
export class Xl9Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-xl-9');
  }
}
