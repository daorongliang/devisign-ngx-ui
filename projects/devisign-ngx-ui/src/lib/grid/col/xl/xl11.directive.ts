import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xl11]'
})
export class Xl11Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-xl-11');
  }
}
