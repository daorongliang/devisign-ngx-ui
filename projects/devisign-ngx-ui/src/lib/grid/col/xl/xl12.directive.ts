import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xl12]'
})
export class Xl12Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-xl-12');
  }
}
