import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xl7]'
})
export class Xl7Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-xl-7');
  }
}
