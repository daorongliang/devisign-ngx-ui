import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xl2]'
})
export class Xl2Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-xl-2');
  }
}
