import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[xl4]'
})
export class Xl4Directive {
  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('col-xl-4');
  }
}
