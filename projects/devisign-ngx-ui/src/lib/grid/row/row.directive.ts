import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[row]'
})
export class RowDirective {

  constructor(
    private _el: ElementRef<Element>
  ) {
    this._el.nativeElement.classList.add('row');
    this._el.nativeElement.classList.add('col-12');
    this._el.nativeElement.classList.add('mx-0');
    this._el.nativeElement.classList.add('px-0');
  }
}
