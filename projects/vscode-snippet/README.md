# Devisign Ngx UI Snippets

Angular 8 Fully Supported.

Gitlab: [https://gitlab.com/daorongliang/devisign-ngx-ui](https://gitlab.com/daorongliang/devisign-ngx-ui)

NPM: [https://www.npmjs.com/package/devisign-ngx-ui](https://www.npmjs.com/package/devisign-ngx-ui)

## Documentation

- Grid System
- Toast Service


### Grid System
- app.module.ts
```ts
import { DeviToastModule, DeviGridModule } from 'devisign-ngx-ui'

...
  imports: [
    DeviGridModule,
  ],
...

```
- html

```html
<!-- Input: d-row -->
<!-- Output: -->
<d-row>
  <d-col xs12>

  </d-col>
</d-col>
`

<!-- Input: 'd-col' -->
<!-- Output: -->
<d-col xs12>

</d-col>
```

## Contributing
Gitlab: https://gitlab.com/daorongliang/devisign-ngx-ui

Join requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License
[MIT](https://choosealicense.com/licenses/mit/)
